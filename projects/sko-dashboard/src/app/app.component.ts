import { Component } from '@angular/core';

@Component({
  selector: 'sko-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sko-dashboard';
}
